<h1 class="page-header">
    <?php echo $college->Id != null ? $college->Colegio : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=colegio">Colegio</a></li>
  <li class="active"><?php echo $college->Id != null ? $college->Colegio : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-colegio" action="?c=colegio&a=Editar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="Id" value="<?php echo $college->Id; ?>" />

    <div class="form-group">
        <label>Colegio</label>
        <input type="text" name="Colegio" value="<?php echo $college->Colegio; ?>" class="form-control" placeholder="Ingrese nombre del Colegio" data-validacion-tipo="requerido|min:20" />
    </div>

    <div class="form-group">
        <label>Domicilio</label>
        <input type="text" name="Domicilio" value="<?php echo $college->Domicilio; ?>" class="form-control" placeholder="Ingrese domicilio del Colegio" data-validacion-tipo="requerido|min:100" />
    </div>

    <hr />

    <div class="text-right">
        <button class="btn btn-success">Actualizar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-producto").submit(function(){
            return $(this).validate();
        });
    })
</script>
