<h1 class="page-header">
    Nuevo Registro
</h1>

<ol class="breadcrumb">
  <li><a href="?c=colegio">Colegio</a></li>
  <li class="active">Nuevo Registro</li>
</ol>

<form id="frm-colegio" action="?c=colegio&a=Guardar" method="post" enctype="multipart/form-data">

    <div class="form-group">
      <label>Nombre del Colegio</label>
      <input type="text" name="Colegio" value="<?php echo $college->Colegio; ?>" class="form-control" placeholder="Ingrese el nombre del Colegio" data-validacion-tipo="requerido|min:20" />
    </div>

    <div class="form-group">
        <label>Domicilio</label>
        <input type="text" name="Domicilio" value="<?php echo $college->Domicilio; ?>" class="form-control" placeholder="Ingrese domicilio del Colegio" data-validacion-tipo="requerido|min:20" />
    </div>

    <hr />

    <div class="text-right">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-colegio").submit(function(){
            return $(this).validate();
        });
    })
</script>
