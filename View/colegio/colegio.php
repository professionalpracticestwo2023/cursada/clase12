<h1 class="page-header">Colegios </h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=votantes&a=Nuevo">Nuevo Votante</a>
    <a class="btn btn-primary" href="?c=colegio&a=Nuevo">Nuevo Colegio</a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th style="width:180px;">Id Colegio</th>
            <th style="width:120px;">Nombre</th>
            <th style="width:120px;">Domicilio</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r->Id; ?></td>
            <td><?php echo $r->Colegio; ?></td>
            <td><?php echo $r->Domicilio; ?></td>
            <td>
                <a href="?c=colegio&a=Crud&Id=<?php echo $r->Id; ?>">Editar</a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=colegio&a=Eliminar&Id=<?php echo $r->Id; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
