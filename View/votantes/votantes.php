<h1 class="page-header">Votantes</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=votantes&a=Nuevo">Nuevo Votante</a>
    <a class="btn btn-primary" href="?c=colegio&a=Nuevo">Nuevo Colegio</a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th style="width:50px;">Documento</th>
            <th style="width:120px;">Apellido</th>
            <th style="width:120px;">Nombre</th>
            <th style="width:120px;">Mesa</th>
            <th style="width:280px;">Colegio</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r->dni; ?></td>
            <td><?php echo $r->apellidos; ?></td>
            <td><?php echo $r->nombres; ?></td>
            <td><?php echo $r->mesa; ?></td>
            <td><?php echo $r->escuela; ?></td>
            <td>
                <a href="?c=votantes&a=Crud&Documento=<?php echo $r->dni; ?>">Editar</a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=votantes&a=Eliminar&Documento=<?php echo $r->dni; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
