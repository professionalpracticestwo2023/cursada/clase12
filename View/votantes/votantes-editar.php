<h1 class="page-header">
    <?php echo $pvd->dni ? $pvd->apellidos : 'Modificar Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=votantes">Votantes</a></li>
  <li class="active"><?php echo $pvd->dni != null ? $pvd->apellidos : 'Editar Registro'; ?></li>
</ol>

<form id="frm-votante" action="?c=votantes&a=Editar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="Documento" value="<?php echo $pvd->dni; ?>" />

    <div class="form-group">
        <label>Apellido</label>
        <input type="text" name="Apellido" value="<?php echo $pvd->apellidos; ?>" class="form-control" placeholder="Ingrese Apellido del votante" data-validacion-tipo="requerido|min:100" />
    </div>

    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="Nombre" value="<?php echo $pvd->nombres; ?>" class="form-control" placeholder="Ingrese Nombre del votante" data-validacion-tipo="requerido|min:100" />
    </div>

    <div class="form-group">
        <label>Mesa</label>
        <input type="text" name="Mesa" value="<?php echo $pvd->mesa; ?>" class="form-control" placeholder="Ingrese mesa del votante" data-validacion-tipo="requerido|min:10" />
    </div>

    <hr />

    <div class="text-right">
        <button class="btn btn-success">Actualizar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-votante").submit(function(){
            return $(this).validate();
        });
    })
</script>
