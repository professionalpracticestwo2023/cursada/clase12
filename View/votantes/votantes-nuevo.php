<h1 class="page-header">
    Nuevo Registro
</h1>

<ol class="breadcrumb">
  <li><a href="?c=votantes">Votantes</a></li>
  <li class="active">Nuevo Registro</li>
</ol>

<form id="frm-votante" action="?c=votantes&a=Guardar" method="post" enctype="multipart/form-data">

    <div class="form-group">
      <label>Documento</label>
      <input type="text" name="Documento" value="<?php echo $pvd->Documento; ?>" class="form-control" placeholder="Ingrese Documento del Votante" data-validacion-tipo="requerido|min:20" />
    </div>

    <div class="form-group">
        <label>Apellido</label>
        <input type="text" name="Apellido" value="<?php echo $pvd->Apellido; ?>" class="form-control" placeholder="Ingrese Apellido del Votante" data-validacion-tipo="requerido|min:100" />
    </div>

    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="Nombre" value="<?php echo $pvd->Nombre; ?>" class="form-control" placeholder="Ingrese Nombre del Votante" data-validacion-tipo="requerido|min:100" />
    </div>

    <div class="form-group">
        <label>Mesa</label>
        <input type="text" name="Mesa" value="<?php echo $pvd->Mesa; ?>" class="form-control" placeholder="Ingrese Mesa del Votante" data-validacion-tipo="requerido|min:10" />
    </div>

    <hr />

    <div class="text-right">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-votante").submit(function(){
            return $(this).validate();
        });
    })
</script>
