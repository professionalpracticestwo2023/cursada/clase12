<?php
class colegio
{
	private $pdo;
	public $Id;
    public $Colegio;
    public $Domicilio;
    

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT Id, Colegio , Domicilio FROM Colegio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($Id)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM Colegio WHERE Id = ?");
			$stm->execute(array($Id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($Id)
	{
		try
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM Colegio WHERE Id = ?");

			$stm->execute(array($Id));
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Actualizar($data)
	{
		try
		{
			$sql = "UPDATE Colegio SET
						Colegio   = ?,
						Domicilio = ?
           	    		WHERE Id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->Colegio,
                        $data->Domicilio,
						$data->Id
                    )
				);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Registrar(colegio $data)
	{
		try
		{
		$sql = "INSERT INTO Colegio (Colegio,Domicilio)
		        VALUES (?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->Colegio,
                    $data->Domicilio
                )
			);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}
}
