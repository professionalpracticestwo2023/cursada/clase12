<?php
class votante
{
	//Atributo para conexión a SGBD
	private $pdo;

		//Atributos del objeto votante
    public $dni;
    public $apellidos;
    public $nombres;
    public $mesa;
	public $escuela;

	//Método de conexión a SGBD.
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Este método selecciona todas las tuplas de la tabla
	//votante en caso de error se muestra por pantalla.
	public function Listar()
	{
		try
		{
			$result = array();
			//Sentencia SQL para selección de datos.
			$stm = $this->pdo->prepare("SELECT dni,apellidos,nombres,mesa,escuela FROM padron order by dni limit 1000");
			//Ejecución de la sentencia SQL.
			$stm->execute();
			//fetchAll — Devuelve un array que contiene todas las filas del conjunto
			//de resultados
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			//Obtener mensaje de error.
			die($e->getMessage());
		}
	}

	//Este método obtiene los datos del votante a partir del documento
	//utilizando SQL.
	public function Obtener($Documento)
	{
		
		try
		{
			//Sentencia SQL para selección de datos utilizando
			//la clausula Where para especificar el documento del votante.
			$stm = $this->pdo->prepare("SELECT * FROM padron WHERE dni = ?");
			//Ejecución de la sentencia SQL utilizando el parámetro documento.
			$stm->execute(array($Documento));
			
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Este método elimina la tupla votante dado un Documento.
	public function Eliminar($Documento)
	{

		try
		{
			//Sentencia SQL para eliminar una tupla utilizando
			//la clausula Where.
			$stm = $this->pdo
			            ->prepare("DELETE FROM padron WHERE dni = ?");

			$stm->execute(array($Documento));
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Método que actualiza una tupla a partir de la clausula
	//Where y el Documento del votante.
	public function Actualizar($data)
	{
		
		try
		{
			//Sentencia SQL para actualizar los datos.
			$sql = "UPDATE padron SET
						apellidos          = ?,
						nombres        = ?,
            			mesa        = ?
				    WHERE dni = ?";
			//Ejecución de la sentencia a partir de un arreglo.
			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->apellidos,
                        $data->nombres,
                        $data->mesa,
                        $data->dni,
					)
				);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Método que registra un nuevo votante a la tabla.
	public function Registrar(votante $data)
	{
		try
		{
			//Sentencia SQL.
			$sql = "INSERT INTO padron (dni,apellidos,nombres,mesa)
		        VALUES (?, ?, ?, ?)";

			$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->dni,
                    $data->apellidos,
                    $data->nombres,
                    $data->mesa,
					
                )
			);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}
}
