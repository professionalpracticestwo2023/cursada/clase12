<?php
require_once 'Model/colegio.php';

class ColegioController{

    private $model;

    public function __CONSTRUCT(){
        $this->model = new colegio();
    }

    //Llamado plantilla principal
    public function Index(){
        require_once 'View/header.php';
        require_once 'View/colegio/colegio.php';
        require_once 'View/footer.php';
    }

    public function Crud(){
        $college = new colegio();

        if(isset($_REQUEST['Id'])){
            $college = $this->model->Obtener($_REQUEST['Id']);
        }

        require_once 'View/header.php';
        require_once 'View/colegio/colegio-editar.php';
        require_once 'View/footer.php';
    }

    public function Nuevo(){
        $college = new colegio();

        require_once 'View/header.php';
        require_once 'View/colegio/colegio-nuevo.php';
        require_once 'View/footer.php';
    }

    public function Guardar(){
        $college = new colegio();

        $college->Colegio = $_REQUEST['Colegio'];
        $college->Domicilio = $_REQUEST['Domicilio'];

        $this->model->Registrar($college);

        header('Location: index.php?c=colegio');
    }

    public function Editar(){
        $college = new colegio();

        $college->Id = $_REQUEST['Id'];
        $college->Colegio = $_REQUEST['Colegio'];
        $college->Domicilio = $_REQUEST['Domicilio'];
        
        $this->model->Actualizar($college);

        header('Location: index.php?c=colegio');
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['Id']);
        header('Location: index.php');
    }
}
