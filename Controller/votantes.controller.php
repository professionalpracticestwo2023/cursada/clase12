<?php
//Se incluye el modelo donde conectará el controlador.
require_once 'Model/votantes.php';

class VotantesController{

    private $model;

    //Creación del modelo
    public function __CONSTRUCT(){
        $this->model = new votante();
    }

    //Llamado plantilla principal
    public function Index(){
        require_once 'View/header.php';
        require_once 'View/votantes/votantes.php';
        require_once 'View/footer.php';
    }

    //Llamado a la vista votante-editar
    public function Crud(){
        $pvd = new votante();

        //Se obtienen los datos del votante a editar.
        if(isset($_REQUEST['Documento'])){
            $pvd = $this->model->Obtener($_REQUEST['Documento']);
        }

        //Llamado de las vistas.
        require_once 'View/header.php';
        require_once 'View/votantes/votantes-editar.php';
        require_once 'View/footer.php';
  }

    //Llamado a la vista votante-nuevo
    public function Nuevo(){
        $pvd = new votante();

        //Llamado de las vistas.
        require_once 'View/header.php';
        require_once 'View/votantes/votantes-nuevo.php';
        require_once 'View/footer.php';
    }

    //Método que registrar al modelo un nuevo votante.
    public function Guardar(){
        $pvd = new votante();

        //Captura de los datos del formulario (vista).
        $pvd->dni = $_REQUEST['Documento'];
        $pvd->apellidos = $_REQUEST['Apellido'];
        $pvd->nombres = $_REQUEST['Nombre'];
        $pvd->mesa = $_REQUEST['Mesa'];


        //Registro al modelo votante.
        $this->model->Registrar($pvd);

        //header() es usado para enviar encabezados HTTP sin formato.
        //"Location:" No solamente envía el encabezado al navegador, sino que
        //también devuelve el código de status (302) REDIRECT al
        //navegador
        header('Location: index.php');
    }

    //Método que modifica el modelo de un votante.
    public function Editar(){
        $pvd = new votante();

        
        $pvd->apellidos = $_REQUEST['Apellido'];
        $pvd->nombres = $_REQUEST['Nombre'];
        $pvd->mesa = $_REQUEST['Mesa'];
        $pvd->dni = $_REQUEST['Documento'];
        $this->model->Actualizar($pvd);
         //Llamado de las vistas.
        
          header('Location: index.php');
    }

    //Método que elimina la tupla votante con el documento dado.
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['Documento']);
        header('Location: index.php');
    }
}
