# mvcPHP
CRUD MVC con PHP, Bootstrap y MySQL
De la Teoría a la Práctica en Diseño de Bases de Datos

1.	Entendiendo el MVC con un CRUD

1.1 ¿Qué es un CRUD?

En computación CRUD es el acrónimo de Crear, Leer, Actualizar y Borrar (del original en inglés: Create, Read, Update and Delete). Se usa para referirse a las funciones básicas en bases de datos o la capa de persistencia en un software.  En este caso para el desarrollo Web.

1.2 ¿Qué es un Framework CSS?

Un framework es simplemente la forma de organizar el desarrollo de una aplicación, es un esquema (un esqueleto, un patrón).  Los framework CSS facilitan el diseño de interfaz de usuario, para que esta sea capaz de adaptarse a los distintos navegadores, configuraciones o incluso dispositivos; la selección de un framework de apoyo puede ahorrar mucho tiempo y esfuerzo, no son un sustituto para saber cómo funciona el CSS.

Existen diversas alternativas de Framework CSS, en esta práctica se utilizará Twitter Bootstrap, que es un framework o conjunto de herramientas de código abierto para diseño de sitios y aplicaciones Web que se constituye en uno de los más populares de GitHub. Contiene plantillas de diseño con tipografía, formularios, botones, cuadros, menús de navegación y otros elementos de diseño basado en HTML y CSS, así como, extensiones de JavaScript opcionales adicionales [1].

1.3 ¿Qué es PHP?

PHP, acrónimo de Hipertexto PreProcessor, es un lenguaje de programación que se interpreta por un servidor Web.  Los lenguajes del lado del servidor son aquellos que son reconocidos, ejecutados e interpretados por el propio servidor y que se envían al cliente en un formato comprensible para él [2].  

Ventajas fundamentales:
•	El código se encuentra protegido tanto de la manipulación de los usuarios como de la presencia de virus.
•	Se caracteriza por su facilidad de aprendizaje en breve tiempo.
•	Es multiplataforma y no requiere de recursos desmesurados para que funcione.

1.4 ¿Qué es el MVC?

El patrón Modelo-Vista-Controlador (MVC) es un paradigma que divide las partes que conforman una aplicación en el Modelo, las Vistas y los Controladores, permitiendo la implementación por separado de cada elemento, garantizando así la actualización y mantenimiento del software de forma sencilla y en un reducido espacio de tiempo [3].

Modelo: datos y reglas de negocio.
Vista: muestra la información del modelo al usuario.
Controlador: gestiona las entradas del usuario.


### Tareas sugeridas
    - Realizar el CRUD de Colegios, para ello deberán armar la tabla 
    - No realizar el borrado fisico sino logico en todos los casos
    - Mostrar en la búsqueda los activos
    - No permitir modificar la clave princial
    - Crear un login con perfil, en donde solo el administrador pueda eliminar
